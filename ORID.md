Objective:
Today, we learned how to use the Flyway tool to extract Dtos and manage database build scripts. In the afternoon, we had a review session to review the learning results of the past two weeks. Finally, our group gave a second presentation on Continuous integration/Continuous Delivery (CI/CD).

Reflective :
During the group presentation, I felt very nervous and did not explain the part I was responsible for fluently. This made me realize that I need to be better prepared and improve my presentation skills in future speeches or presentations.

Interpretive:
Through today's experience, I learned that preparation and confidence are crucial in a team presentation or presentation. I need to practice more and improve my presentation skills. At the same time, I also realize that the mutual support and cooperation between team members is very valuable.

Decisional:
In order to better express myself in future presentations or speeches, I decided to strengthen the training and practice of presentation skills. I will seek out resources and techniques to learn how to speak more confidently and fluently in public. At the same time, I will also seek advice and support from team members. Through continuous efforts and practice, I believe I can improve my presentation ability and make greater contributions to the success of the team. In addition, I also want to say that our group members are really great!