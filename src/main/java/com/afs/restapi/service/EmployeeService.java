package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAllByStatusTrue();
    }

    public Employee update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id).orElseThrow(EmployeeNotFoundException::new);
        employeeRepository.save(EmployeeMapper.toEntity(employeeUpdateRequest, employee));
        return employee;
    }

    public EmployeeResponse findById(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList();
    }

    public Employee insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeCreateRequest);
        return employeeRepository.save(employee);
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
