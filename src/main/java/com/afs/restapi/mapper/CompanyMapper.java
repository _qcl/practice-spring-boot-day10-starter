package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.CompanyUpdateRequest;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toEntity(CompanyCreateRequest companyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest, company);
        return company;
    }

    public static Company toEntity(CompanyUpdateRequest companyUpdateRequest, Company company) {
        if (companyUpdateRequest.getName() != null) {
            company.setName(companyUpdateRequest.getName());
        }
        return company;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        return companyResponse;
    }
}