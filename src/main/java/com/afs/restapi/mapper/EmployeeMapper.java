package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest, employee);
        return employee;
    }

    public static Employee toEntity(EmployeeUpdateRequest employeeUpdateRequest, Employee employee) {
        if (employeeUpdateRequest.getAge() != null) {
            employee.setAge(employeeUpdateRequest.getAge());
        }
        if (employeeUpdateRequest.getSalary() != null) {
            employee.setSalary(employeeUpdateRequest.getSalary());
        }
        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }
}